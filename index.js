const http = require('http');
const fs = require('fs');
const url = require('url');
const BAD_REQUEST = 400,
    INTERNAL_SERVER_ERROR = 500,
    PORT = 8080;

function checkLogsExistence() {
    return new Promise((resolve, reject) => {
        fs.readFile('./logs.json', 'utf-8', (err, content) => {
            if (err) {
                reject(err);
            }
            resolve(content);
        });
    });
}

function createLogsFile() {
    return new Promise((resolve, reject) => {
        fs.writeFile('./logs.json', '{"logs":[]}', (err) => {
            if (err) {
                reject(err);
            }
        });
        resolve('{"logs":[]}');
    });
}

function checkFilesDirectoryExistence() {
    return new Promise((resolve, reject) => {
        fs.open('./file', (err) => {
            if (err) {
                reject();
            }
            resolve();
        });
    });
}

function addLog(message) {
    const log = {
        time: Date.now(),
        message
    };

    checkLogsExistence()
        .catch((err) => {
            if (err.code === 'ENOENT') {
                return createLogsFile();
            }
        })
        .then((data) => JSON.parse(data))
        .then((logs) => {
            logs.logs.push(log);
            fs.writeFile('./logs.json', JSON.stringify(logs), (err) => {
                if (err) throw err;
            });
        });
}

function checkFilename(filename) {
    const regE = /([a-zA-Z0-9\s_\\.\-():])+(.doc|.txt|.js|.json|.css|.html)$/;
    return regE.test(filename);
}

module.exports = () => {
    http.createServer((req, res) => {
        const {
            pathname,
            query
        } = url.parse(req.url, true);
        if (req.method === 'POST') {
            checkFilesDirectoryExistence()
                .catch(() => {
                    fs.mkdirSync('./file');
                })
                .then(() => {
                    if (!checkFilename(query.filename)) {
                        res.writeHead(BAD_REQUEST, {
                            'Content-type': 'text/html'
                        });
                        res.end(() => {
                            addLog(`The filename "${query.filename}" is incorrect, file could not be saved`);
                        });
                        return;
                    }
                    fs.writeFile(`./file/${query.filename}`, query.content, (err) => {
                        if (err) {
                            res.writeHead(INTERNAL_SERVER_ERROR, {
                                'Content-type': 'text/html'
                            });
                            res.end(`The file "${pathname}" could not be saved`);
                        }
                        res.end(() => {
                            addLog(`${query.filename} file was saved`);
                        });
                    });
                });
        } else if (req.method === 'GET') {
            if (pathname === '/logs') {
                fs.readFile('./logs.json', (err, content) => {
                    if (err) {
                        res.end('There are no logs for now');
                    }
                    if (query.from && query.to) {
                        let filteredByTime = JSON.parse(content);
                        filteredByTime.logs = filteredByTime.logs.filter(el => el.time >= query.from && el.time <= query.to);
                        res.end(JSON.stringify(filteredByTime));
                    }
                    res.end(content);
                });
            } else {
                fs.readFile(`./${pathname}`, (err, content) => {
                    if (err) {
                        res.writeHead(BAD_REQUEST, {
                            'Content-type': 'text/html'
                        });
                        res.end(`No files found with name "${pathname}" found`);
                        addLog(`"${pathname}" file was not found`);
                    }
                    res.end(content, () => addLog(`${pathname} file has been viewed`));
                });
            }
        }
    }).listen(process.env.PORT || PORT);
};